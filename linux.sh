# mount a usb drive with udisksctl
# do not mount with mount because sudo right are needed

# first use lsblk to find the drive (ex: /dev/sda1 or /dev/sbd1)
lsblk

# then mount it with udisksctl
udisksctl mount -b /dev/sda1

# then mount it with udisksctl
udisksctl unmount -b /dev/sda1

